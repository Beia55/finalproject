class Board {
    constructor() {
        this.dimension = 5;
        this.board = [];
        this.pieceSize = 60;
        this.spaceBetweenPieces = 10;
    }

    createBoard() {
        let posXCopy = windowWidth / 2.5;
        let posX = posXCopy;
        let posY = windowHeight / 7;
        // let posY = 150;
        for (let i = 0; i < this.dimension; i++) {
            let line = [];
            posX = posXCopy;
            for (let j = 0; j < this.dimension; j++) {
                let pieceObj = {
                    pieceX: posX,
                    pieceY: posY,
                    pieceObjSize: this.pieceSize,
                    pieceColor: darkColor2,
                    pieceText: ""
                }
                line.push(pieceObj);
                posX += this.pieceSize + this.spaceBetweenPieces;
            }
            posY += this.pieceSize + this.spaceBetweenPieces;
            this.board.push(line);
        }
    }

    displayBoard() {
        for (let i = 0; i < this.dimension; i++) {
            for (let j = 0; j < this.dimension; j++) {
                strokeWeight(1);
                fill(this.board[i][j].pieceColor);
                rect(this.board[i][j].pieceX, this.board[i][j].pieceY, this.board[i][j].pieceObjSize, this.board[i][j].pieceObjSize);
                fill(darkColor1);
                strokeWeight(3);
                textSize(40);
                text(this.board[i][j].pieceText, this.board[i][j].pieceX + 30, this.board[i][j].pieceY + 45);
            }
        }
    }
}

// ---------->>> setup & draw <<<----------

const boardInit = new Board();

function setup() {
    canvas = createCanvas(windowWidth, windowHeight);
    initialView();
    boardInit.createBoard();
}

function draw() {
    if (statusGame == "active") {
        gameView();
    }

}

// ---------->>> rules <<<----------

function exp(){
    // if(isClicked == false){isClicked = true;}
    // if(isClicked == true) {isClicked = false}

    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            if(mouseIsPressed == true){
                if (mouseX >= boardInit.board[i][j].pieceX && mouseX <= boardInit.board[i][j].pieceX + boardInit.pieceSize) {
                    if (mouseY >= boardInit.board[i][j].pieceY && mouseY <= boardInit.board[i][j].pieceY + boardInit.pieceSize) {
                        // boardInit.board[i][j].pieceX = (i+1)*30;
                        // boardInit.board[i][j].pieceY = (j+1)*50;
                        // rect(-1,0,boardInit.pieceSize);
                    }
                }
            }
        }
    }
}


function clickOnPiece() {
    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            if (mouseIsPressed) {
                if (mouseX >= boardInit.board[i][j].pieceX && mouseX <= boardInit.board[i][j].pieceX + boardInit.pieceSize) {
                    if (mouseY >= boardInit.board[i][j].pieceY && mouseY <= boardInit.board[i][j].pieceY + boardInit.pieceSize) {
                        
                    // selectPieceToMove();

                    // rect(-1,0,boardInit.pieceSize);


                        // trebuie sa creez un nou rect

                    //     if(boardInit.board[i][j].pieceText == "X"){
                    //     boardInit.board[i][j].pieceColor = ligthColor2;
                    //     // boardInit.board[0][1].pieceColor = darkColor1;
                    //    } else {
                    //     boardInit.board[i][j].pieceColor = darkColor1;
                    //    }

                    }
                }
            }
        }
    }
}

function writeXVsY(){
    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            // if(isClicked == true){
                if (mouseX >= boardInit.board[i][j].pieceX && mouseX <= boardInit.board[i][j].pieceX + boardInit.pieceSize) {
                    if (mouseY >= boardInit.board[i][j].pieceY && mouseY <= boardInit.board[i][j].pieceY + boardInit.pieceSize) {                    
                        if(mouseIsPressed == true){
                        // changeIsClicked();
                            if(boardInit.board[i][j].pieceText === ""){
                                if(i == 0 || i == 4 || j == 0 || j == 4){
                                // boardInit.board[i][j].pieceText = playerX;
                                // changePayer();
                                // clickOnPiece();   
                                // selectPieceToMove();

                                fill(darkColor3);
                                if(j != 0){
                                    rect(boardInit.board[i][0].pieceX-(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.board[i][0].pieceY,boardInit.pieceSize,boardInit.pieceSize);
                                   
                                    text("x",boardInit.board[i][0].pieceX-(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.board[i][0].pieceY,boardInit.pieceSize);
                                }
                                if(j != 4){
                                    rect(boardInit.board[i][4].pieceX+(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.board[i][4].pieceY,boardInit.pieceSize,boardInit.pieceSize);
                                    text("x",boardInit.board[i][4].pieceX+(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.board[i][4].pieceY,boardInit.pieceSize)
                                }
                                if(i != 4){
                                    rect(boardInit.board[4][j].pieceX,boardInit.board[4][j].pieceY+(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.pieceSize,boardInit.pieceSize);
                                    text("x",boardInit.board[4][j].pieceX,boardInit.board[4][j].pieceY+(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.pieceSize)
                                }
                                if(i != 0){
                                    rect(boardInit.board[0][j].pieceX,boardInit.board[0][j].pieceY-(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.pieceSize,boardInit.pieceSize);
                                    text("x",boardInit.board[0][j].pieceX,boardInit.board[0][j].pieceY-(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.pieceSize)
                                }
                                }

                                changePayer();
                            }
                        }
                    }
                }
            // }
        }
    }
}

function apasa(){
    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            if (mouseX >= boardInit.board[i][j].pieceX && mouseX <= boardInit.board[i][j].pieceX + boardInit.pieceSize) {
                if (mouseY >= boardInit.board[i][j].pieceY && mouseY <= boardInit.board[i][j].pieceY + boardInit.pieceSize) {                    
                    
                    if(isClicked == true){
                        isClicked = false;
                    }
                    if(isClicked == false){
                        isClicked = true;
                    }
                    if(isClicked == true){
                        fill(darkColor3);
                        rect(boardInit.board[i][0].pieceX-(boardInit.pieceSize+boardInit.spaceBetweenPieces),boardInit.board[i][0].pieceY,boardInit.pieceSize,boardInit.pieceSize);
                    }
                }
            }
        }
    }
}

function  changePayer(){
    let playerCopy = "X";
    if(playerX == playerCopy){
        playerX = "O";
    } else {
        playerX = "X";
    }
}

function selectPieceToMove(){
    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            if(isClicked == true){
                boardInit.board[i][j].pieceX = i*boardInit.board[i][j].pieceObjSize;
                boardInit.board[i][j].pieceY = j-1*boardInit.board[i][j].pieceObjSize;
            }
        }
    }
}

function showsThePossibleMove() {
    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            if(i==0){
                
            }
            if(j==0){

            }
            
            
            boardInit.board[i][j].pieceColor = darkColor3;
            boardInit.board[i][j];
        }
    }
}


// function spliceBoard(){
//     for (let i = 0; i < boardInit.dimension; i++) {
//         for (let j = 0; j < boardInit.dimension; j++) {
//             split();
//         }
//     }
// }