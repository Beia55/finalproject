// ------->>> GLOBAL VARIABLES <<<-------
var namePlayer1, namePlayer2;
var playerX = "X", playerO = "O"; 
var inpName1, inpName2;
var statusGame = "inactive";
var isClicked = false;

// ---------->>> COLORS <<<----------
var darkColor1 =  'rgb(5, 0, 61)';
var darkColor2 = 'rgb(107, 26, 24)';
var darkColor3 = 'rgb(70, 30, 30)';
var ligthColor1 = 'rgb(174, 169, 186)';
var ligthColor2 = 'rgb(122, 117, 133)';

// --------->>> DIMENSIONS <<<---------
var titleSize = 130;
var btnsSize = 70;