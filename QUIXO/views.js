// ---------->>> First/Initial View <<<----------
function initialView(){
    background(darkColor1)
    
    fill(darkColor2);
    textAlign(CENTER);
    textSize(titleSize);
    stroke(ligthColor1);
    strokeWeight(5);
    text("QUIXO", windowWidth/2-titleSize/2, windowHeight/2-titleSize/2);
    
    startBTN();
}

function startBTN(){
    btnStart = createButton("Start");
    btnStart.position( windowWidth/2-titleSize-btnsSize/2, windowHeight/2+btnsSize/2);
   
    btnStart.style('background-color',darkColor2);
    btnStart.style('padding', btnsSize/10 + 'px ' + btnsSize/2 + 'px');
    btnStart.style('font-size', btnsSize+'px');
    btnStart.style('color', ligthColor1);
    btnStart.style('border', 3 + 'px solid ' + ligthColor1);
    btnStart.style('border-radius', btnsSize/5 + 'px');
    btnStart.style('cursor', 'pointer');
   
    btnStart.mouseOver(btnMouseOn);
    btnStart.mouseOut(btnMouseOut);
    btnStart.mousePressed(detailsView);
}

// ---------->>> Second/Min-Form View <<<----------
function detailsView(){
    btnStart.remove();
    clear();

    background(darkColor1);
    nameInput();

    btnNames = createButton("Start GAME"); 
    btnNames.position(windowWidth/2+btnsSize, windowHeight/2-btnsSize*1.3); 
   
    btnNames.style('background-color',ligthColor1);
    btnNames.style('padding', btnsSize/10 + 'px ' + btnsSize/2 + 'px');
    btnNames.style('font-size', btnsSize+'px');
    btnNames.style('color', darkColor2);
    btnNames.style('border', 3 + 'px solid ' + darkColor2);
    btnNames.style('border-radius', btnsSize/5 + 'px');
    btnNames.style('cursor', 'pointer');

    btnNames.mouseOver(btnMouseOn);
    btnNames.mouseOut(btnMouseOut);
    btnNames.mousePressed(gameStatusView);
}

function nameInput(){
    textSize(titleSize/3);
    text("Name 1:",windowWidth/6-btnsSize, windowHeight/2-btnsSize*1.3);
    inpName1 = createInput();
    inpName1.position(windowWidth/4-btnsSize, windowHeight/2-btnsSize*2);
    inpName1.style('width', btnsSize*5+"px");
    inpName1.style('background-color', ligthColor1);
    inpName1.style('font-size', titleSize/2+'px');
    inpName1.style('color', darkColor1);
    inpName1.style('border', 3 + 'px solid ' + darkColor2);
    inpName1.style('border-radius', btnsSize/3 + 'px');
   
    btnStart.mouseOver(btnMouseOn);
    btnStart.mouseOut(btnMouseOut);

    text("Name 2:",windowWidth/6-btnsSize, windowHeight/2+btnsSize/10);
    inpName2 = createInput();
    inpName2.position(windowWidth/4-btnsSize, windowHeight/2-btnsSize/2);
    inpName2.style('width', btnsSize*5+"px");
    inpName2.style('background-color',ligthColor1);
    inpName2.style('font-size', titleSize/2+'px');
    inpName2.style('color', darkColor1);
    inpName2.style('border', 3 + 'px solid ' + darkColor2);
    inpName2.style('border-radius', btnsSize/3 + 'px');
   
    btnStart.mouseOver(btnMouseOn);
    btnStart.mouseOut(btnMouseOut);
}

// ---------->>> Third/Play-Game View <<<----------


function gameView(){
    inpName1.remove();
    inpName2.remove();
    btnNames.remove();
    // clear();

    background(darkColor1);
    reloadGameBTN();
    refreshPageBTN();
    displayPlayer1();
    
    boardInit.displayBoard();
    // boardInit.clieckOnEachPiece();
    // boardInit.clickOnPiece();
    // clickOnPiece();

    // apasa();

    // mousePressed();

    writeXVsY(); 
    // clickToShowPossibleMouves();
    // mousePressed();
}

function displayPlayer1(){
    textSize(titleSize/3);
    text(inpName1.value(), 150, 150);
    text(inpName2.value(), 150, 200);
}

function reloadGameBTN(){
    reloadBTN = createButton("Reaload Game");
    reloadBTN.position( windowWidth/13, windowHeight/1.1);
   
    reloadBTN.style('background-color',darkColor2);
    reloadBTN.style('padding', btnsSize/13 + 'px ' + btnsSize/3 + 'px');
    reloadBTN.style('font-size', btnsSize/5+'px');
    reloadBTN.style('color', ligthColor1);
    reloadBTN.style('border', 3 + 'px solid ' + ligthColor1);
    reloadBTN.style('border-radius', btnsSize/5 + 'px');
    reloadBTN.style('cursor', 'pointer');
   
    reloadBTN.mouseOver(btnMouseOn);
    reloadBTN.mouseOut(btnMouseOut);
    reloadBTN.mousePressed(reloadGame);
}

function refreshPageBTN(){
    refreshBTN = createButton("Refresh page");
    refreshBTN.position( windowWidth/5, windowHeight/1.1);
   
    refreshBTN.style('background-color',darkColor2);
    refreshBTN.style('padding', btnsSize/13 + 'px ' + btnsSize/3 + 'px');
    refreshBTN.style('font-size', btnsSize/5+'px');
    refreshBTN.style('color', ligthColor1);
    refreshBTN.style('border', 3 + 'px solid ' + ligthColor1);
    refreshBTN.style('border-radius', btnsSize/5 + 'px');
    refreshBTN.style('cursor', 'pointer');

    refreshBTN.mouseOver(btnMouseOn);
    refreshBTN.mouseOut(btnMouseOut);
    refreshBTN.mousePressed(refreshPage);
}

// ---------->>> Last/Game-Over View <<<----------
function lastView(){
    clear();
    background(darkColor1);
    textAlign(CENTER);
    textSize(titleSize);
    stroke(ligthColor1);
    strokeWeight(5);
    text("GAME OVER", windowWidth/2-titleSize/2, windowHeight/2-titleSize/2);
    text("The winner is: ", windowWidth/2-titleSize/2, windowHeight/5-titleSize/2);

    restartBTN();
}

function restartGameBTN(){
    restartBTN = createButton("RestartGame");
    restartBTN.position( windowWidth/2-titleSize-btnsSize/2, windowHeight/2+btnsSize/2);
   
    restartBTN.style('background-color',darkColor2);
    restartBTN.style('padding', btnsSize/10 + 'px ' + btnsSize/2 + 'px');
    restartBTN.style('font-size', btnsSize+'px');
    restartBTN.style('color', ligthColor1);
    restartBTN.style('border', 3 + 'px solid ' + ligthColor1);
    restartBTN.style('border-radius', btnsSize/5 + 'px');
    restartBTN.style('cursor', 'pointer');

    restartBTN.mouseOver(btnMouseOn);
    restartBTN.mouseOut(btnMouseOut);

    restartBTN.mousePressed(refreshPage);
}

// ---------->>> General Style-Elements && Function <<<----------

function btnMouseOn(){
    this.style('filter', 'brightness(130%)');
}

function btnMouseOut(){
    this.style('filter', 'brightness(100%)');
}

function refreshPage(){
    location.reload(true);
}

function reloadGame(){
    // location.reload(true);
    for (let i = 0; i < boardInit.dimension; i++) {
        for (let j = 0; j < boardInit.dimension; j++) {
            boardInit.board[i][j].pieceText = ""; 
            boardInit.board[i][j].pieceColor = darkColor2; 
        }
    }
}

function gameStatusView(){
    if(statusGame == "inactive"){
        statusGame = "active";
    }
}